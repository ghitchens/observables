defmodule Observables do
  @moduledoc ~S"""
  """

  use GenServer

  ## API

  def start_link(options \\ []) do
    GenServer.start_link(__MODULE__, options, name: __MODULE__)
  end

  def stop() do
    GenServer.stop(__MODULE__)
  end

  def update(key, value) do
    GenServer.call(__MODULE__, {:update, key, value})
  end

  def get(key) do
    GenServer.call(__MODULE__, {:get, key})
  end

  def find(matchspec) do
    GenServer.call(__MODULE__, {:find, matchspec})
  end

  def register(matchspec) do
    GenServer.call(__MODULE__, {:register, matchspec})
  end

  def unregister(matchspec) do
    GenServer.call(__MODULE__, {:unregister, matchspec})
  end

  ## Callbacks

  defmodule State do
    defstruct kv_ets: nil,
      match_ets: nil,
      pid_ets: nil
  end

  defp kvtable_name() do
   Module.concat(__MODULE__, "KVTable") 
  end

  defp matchtable_name() do
    Module.concat(__MODULE__, "Match") 
  end

  defp pidtable_name() do
    Module.concat(__MODULE__, "Pid") 
  end

  def init(_options) do
    Process.flag(:trap_exit, true)

    kv_ets = :ets.new(kvtable_name(), [:set])
    match_ets = :ets.new(matchtable_name(), [:set])
    pid_ets = :ets.new(pidtable_name(), [:duplicate_bag])

    {:ok, %State{kv_ets: kv_ets, match_ets: match_ets, pid_ets: pid_ets}}
  end

  def handle_call({:update, key, value}, {pid, _}, state) do
    true = :ets.insert(state.kv_ets, {key, {pid, value}})
    run_all_matchspecs(state, key, value)
    {:reply, :ok, state}
  end
  def handle_call({:register, matchspec}, {pid, _}, state) do
    true = :ets.insert(state.match_ets, {matchspec, pid})
    run_matchspec(state.kv_ets, matchspec, pid)
    {:reply, :ok, state}
  end
  def handle_call({:unregister, matchspec}, {pid, _}, state) do
    true = :ets.delete_object(state.match_ets, {matchspec, pid})
    {:reply, :ok, state}
  end

  def handle_call({:find, matchspec}, _from, state) do
    {:reply, find_matches(state.kv_ets, matchspec), state}
  end
  def handle_call({:get, key}, _from, state) do
    value = case :ets.lookup(state.kv_ets, key) do
              # Only handle the one result case now. Two+ results really seems wrong.
              [{^key, {_pid, value}}] -> value
              _ -> nil
            end
    {:reply, value, state}
  end

  def handle_info({:EXIT, pid, _reason}, ets) do
    entries = :ets.take(ets, pid)
    for {_pid, key, key_ets} <- entries do
      try do
        :ets.match_delete(key_ets, {key, {pid, :_}})
      catch
        :error, :badarg -> :badarg
      end
    end
    {:noreply, ets}
  end
  def handle_info(msg, state) do
    super(msg, state)
  end

  def run_all_matchspecs(state, key, value) do
    for {matchspec, pid} <- :ets.select(state.match_ets, [{:_, [], [:'$_']}]) do
      case is_match(key, matchspec) do
        true -> send(pid, [{key, value}])
        false -> :ok
      end
    end
  end
  def run_matchspec(kv_ets, matchspec, pid) do
    case find_matches(kv_ets, matchspec) do
      [] -> :ok
      results ->
        send(pid, results)
    end
  end

  def find_matches(kv_ets, matchspec) do
    for {key, {_pid, value}} <- :ets.select(kv_ets, [{{matchspec, {:_, :_}}, [], [:'$_']}]) do
      {key, value}
    end
  end

  def is_match({ a,  b,  c}, {a,  b, c}), do: true
  def is_match({ a,  b, _c}, {a,  b, :_}), do: true
  def is_match({ a, _b,  c}, {a,  :_, c}), do: true
  def is_match({ a, _b, _c}, {a,  :_, :_}), do: true
  def is_match({_a,  b,  c}, {:_, b, c}), do: true
  def is_match({_a,  b, _c}, {:_, b, :_}), do: true
  def is_match({_a, _b,  c}, {:_, :_, c}), do: true
  def is_match({_a, _b, _c}, {:_, :_, :_}), do: true
  def is_match(_, _), do: false
end
