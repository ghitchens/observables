defmodule ObservablesTest do
  use ExUnit.Case

  test "update and get work with one item" do
    {:ok, _} = Observables.start_link()
    assert Observables.get({:net, "eth0", :ipv4_address}) == nil
    assert Observables.find({:net, "eth0", :ipv4_address}) == []

    assert Observables.update({:net, "eth0", :ipv4_address}, "127.0.0.1") == :ok
    assert Observables.get({:net, "eth0", :ipv4_address}) == "127.0.0.1"
    assert Observables.find({:net, "eth0", :ipv4_address}) == [{{:net, "eth0", :ipv4_address}, "127.0.0.1"}]
  end

  test "update and get work with multiple items" do
    {:ok, _} = Observables.start_link()
    assert Observables.update({:net, "eth0", :ipv4_address}, "127.0.0.1") == :ok
    assert Observables.update({:net, "wlan0", :ipv4_address}, "127.0.0.1") == :ok
    assert Observables.update({:net, "wlan0", :something_else}, 4) == :ok

    # Get all IPv4 addresses
    assert Observables.find({:net, :_, :ipv4_address}) ==
      [{{:net, "eth0", :ipv4_address}, "127.0.0.1"}, {{:net, "wlan0", :ipv4_address}, "127.0.0.1"}]
  end

  test "notification when register" do
    {:ok, _} = Observables.start_link()

    # Set the IP address
    assert Observables.update({:net, "eth0", :ipv4_address}, "127.0.0.1") == :ok

    # Someone comes in a registers after the interface is configured
    # to get all eth0 IP address changes
    assert Observables.register({:net, "eth0", :ipv4_address})

    # Make sure that we get a notification
    assert_receive [{{:net, "eth0", :ipv4_address}, "127.0.0.1"}]
  end

  test "notification on update" do
    {:ok, _} = Observables.start_link()

    # Register before the IP address is set
    assert Observables.register({:net, "eth0", :ipv4_address})

    # Set the IP address for something uninteresting
    assert Observables.update({:net, "wlan0", :ipv4_address}, "127.0.0.3") == :ok

    # Set the IP address
    assert Observables.update({:net, "eth0", :ipv4_address}, "127.0.0.2") == :ok

    # Make sure that we get a notification
    assert_receive [{{:net, "eth0", :ipv4_address}, "127.0.0.2"}]
  end

  test "wildcard notifications" do
    {:ok, _} = Observables.start_link()

    # Register for all IP address changes
    assert Observables.register({:net, :_, :ipv4_address})

    # Check that we get updates for wlan0
    assert Observables.update({:net, "wlan0", :ipv4_address}, "127.0.0.4") == :ok
    assert_receive [{{:net, "wlan0", :ipv4_address}, "127.0.0.4"}]

    # Check that we get updates for eth0
    assert Observables.update({:net, "eth0", :ipv4_address}, "127.0.0.5") == :ok
    assert_receive [{{:net, "eth0", :ipv4_address}, "127.0.0.5"}]
  end

  test "batch wildcard notification on init" do
    {:ok, _} = Observables.start_link()

    # IP address notifications go out before register is called
    assert Observables.update({:net, "wlan0", :ipv4_address}, "127.0.0.6") == :ok
    assert Observables.update({:net, "eth0", :ipv4_address}, "127.0.0.7") == :ok

    # Register for all IP address changes
    assert Observables.register({:net, :_, :ipv4_address})

    # Get two notifications (order not guaranteed except for this test)
    assert_receive [{{:net, "eth0", :ipv4_address}, "127.0.0.7"},
                    {{:net, "wlan0", :ipv4_address}, "127.0.0.6"}]
  end

end
